import * as action_types from './action_types';

export const ADD_TODO = {
  type: action_types.ADD_TODO
}

export const REMOVE_SELECTED_TODOS = {
  type: action_types.REMOVE_SELECTED_TODOS
}

export const setSearchField = (text) => ({
  type: action_types.CHANGE_TEXTFIELD,
  payload: text
})

export const toggleTodoStatus = (uuid) => ({
  type: action_types.TOGGLE_TODO_STATUS,
  payload: uuid
})

export const removeTodo = (uuid) => ({
  type: action_types.REMOVE_TODO,
  payload: uuid
})

export const setTodosFromFirebase = (todos) => ({
  type: action_types.SET_TODOS_FROM_FIREBASE,
  payload: todos
})
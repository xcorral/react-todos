import React from 'react';
import {connect} from 'react-redux';
import * as actions from '../actions/actions';
import getDatabase from '../services/firebaseService';
import * as constants from '../constants';
import uuidv4 from'uuid/v4';

const database = getDatabase();

const mapStateToProps = state => {
  return {
    todos: state.todos,
    currentTodo: state.currentTodo
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    changeTextField: (event) => dispatch(actions.setSearchField(event.target.value)),
    submitTodo: (event,currentTodo) => {
      event.preventDefault();
      const newTodo = {
        uuid: uuidv4(), 
        taskName:currentTodo, 
        status: constants.PENDING_STATUS
      };
      database.ref('todos/' + newTodo.uuid).set(newTodo);
      dispatch(actions.ADD_TODO);
    },
    focusTextField: (event) => event.target.placeholder=''
  }
}

class SearchBox extends React.Component {
  render () {
    console.log('Rendering SearchBox');
    return (
      <div className="searchBox">
        <div className="title">MARINA TO DO's</div>
        <form onSubmit={(event) => this.props.submitTodo(event, this.props.currentTodo)}>
          <input type="text" className="new-task" placeholder="new task" onChange={this.props.changeTextField} value={this.props.currentTodo} onFocus={this.props.focusTextField}></input>
          <button className="new-task-button">ADD</button>        
        </form>
      </div>
    )
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchBox);
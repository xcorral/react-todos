import React from 'react';
import Todo from './Todo';
import { DONE_STATUS, PENDING_STATUS } from '../constants';


class TodoList extends React.Component {
  
  render() {
    console.log('Rendering TodosList');
    console.log(this.props.todos)
    if (this.props.todos.length === 0) {
      return <div>No tasks... yet</div>;
    } else {
      let lastStatus = PENDING_STATUS;
      return (
        <ul>
          {
            this.props.todos.map( (todo,i) => {
              if (todo.status === DONE_STATUS && lastStatus === PENDING_STATUS) {
                lastStatus = DONE_STATUS;
                return (
                    <Todo key={todo.taskName} todo={todo} lineBreak={true} />  
                )                  
              }  
              return <Todo key={todo.taskName} todo={todo} />
            })
          }
        </ul>
      )
    }
  }
}

export default TodoList;
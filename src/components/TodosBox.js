import React from 'react';
import {connect} from 'react-redux';
import TodoList from './TodoList';
import * as actions from '../actions/actions';
import getDatabase from '../services/firebaseService';
import * as constants from '../constants';

const mapStateToProps = state => {
  return {
    todos: state.todos
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    readAllTodosFromFirebase: (todos) => dispatch(actions.setTodosFromFirebase(todos)),
    removeSelectedTodos: (event,todos) => {
      event.preventDefault();
      todos.filter(todo => todo.status === constants.DONE_STATUS).map((todo) => database.ref('todos/' + todo.uuid).remove());
      return dispatch(actions.REMOVE_SELECTED_TODOS)
    }
  }
}

const database = getDatabase();

class TodosBox extends React.Component {

  todos = [];

  componentWillMount() {
      database.ref('todos').on('value', (snapshot) => {
        const newTodos = snapshot.val() || [];
        const todos = Object.values(newTodos);
        this.props.readAllTodosFromFirebase(todos);
      });
  }

  render() {
    console.log('Rendering TodosBox');
    return (
      <div className="tasks">
        <TodoList todos={this.props.todos}></TodoList>
        <a href="/" className="clear-button" onClick={(event) => this.props.removeSelectedTodos(event, this.props.todos)}>Clear Done</a>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodosBox);
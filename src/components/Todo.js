import React from 'react';
import {connect} from 'react-redux';
import {toggleTodoStatus, removeTodo} from '../actions/actions';
import * as constants from '../constants';
import getDatabase from '../services/firebaseService';
import { FaTimes } from "react-icons/fa";

const database = getDatabase();

const mapStateToProps = state => (state);

const mapDispatchToProps = (dispatch, props) => {
  return {
    onToggleState: () => {
      let currentTodo = props.todo;
      if (currentTodo.status === constants.DONE_STATUS) {
        currentTodo.status = constants.PENDING_STATUS;
      } else {
        currentTodo.status = constants.DONE_STATUS;
      }
      console.log(currentTodo);
      database.ref('todos/' + props.todo.uuid).update(currentTodo);
      return dispatch(toggleTodoStatus(props.todo.uuid))
    },
    onDeleteTodo: (event) => {
      database.ref('todos/' + props.todo.uuid).remove();
      dispatch(removeTodo(props.todo.uuid));
    }
  }
}

class Todo extends React.Component {

  render() {
    console.log('Rendering Todo');
    this.props.todo.statusClass = this.props.todo.status === constants.DONE_STATUS ? 'todo-text stroke' : 'todo-text';
    let todoClass = this.props.lineBreak ? 'lineBreak' : '';
    return (
      <li className={todoClass}>
        <div className='task'>
          <FaTimes onClick={this.props.onDeleteTodo} data={this.props.todo.uuid}></FaTimes>          
          <span className={this.props.todo.statusClass} onClick={this.props.onToggleState}>{this.props.todo.taskName}</span>
        </div>
      </li>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Todo);
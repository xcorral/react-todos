import * as firebase from 'firebase/app';
import "firebase/database";

const firebaseConfig = {
  apiKey: "AIzaSyA5g2ukATB5o0I2okQVqbCvalBT02fP6Fo",
    authDomain: "keepmarina-2f266.firebaseapp.com",
    databaseURL: "https://keepmarina-2f266.firebaseio.com",
    projectId: "keepmarina-2f266",
    storageBucket: "keepmarina-2f266.appspot.com",
    messagingSenderId: "288355080140",
    appId: "1:288355080140:web:739b5dc8e2530b1c"
};

let databaseInstance = null;

const getDatabase = () => {
  console.log('initializing firebase');
  if (databaseInstance === null ) {
    databaseInstance = firebase.initializeApp(firebaseConfig).database();
  }
  return databaseInstance;
}


export default getDatabase;
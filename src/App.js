import React from 'react';
import './scss/App.scss';
import TodosBox from './components/TodosBox';
import SearchBox from './components/SearchBox';
import {connect} from 'react-redux';
import * as firebase from 'firebase/app';

const mapStateToProps = state => {

  return state;
}

const mapDispatchToProps = dispatch => ({})

class App extends React.Component {

  render() {
    console.log('Rendering App');

    /*let provider = new firebase.auth.GoogleAuthProvider();
    provider.addScope('https://www.googleapis.com/auth/contacts.readonly');
    firebase.auth().useDeviceLanguage();

    firebase.auth().signInWithPopup(provider).then(function(result) {
      // This gives you a Google Access Token. You can use it to access the Google API.
      var token = result.credential.accessToken;
      // The signed-in user info.
      var user = result.user;
      // ...
    }).catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // The email of the user's account used.
      var email = error.email;
      // The firebase.auth.AuthCredential type that was used.
      var credential = error.credential;
      // ...
    });

    return (
      <div className="App">
        <div className="wrapper">          
          <SearchBox />
          <TodosBox />
        </div>            
      </div>
    );*/
    return(
      <div class="g-signin2" data-onsuccess="onSignIn"></div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

import * as action_types from '../actions/action_types';
import { DONE_STATUS } from '../constants';
import _ from 'lodash/collection';

const initialState = {
  currentTodo: '',
  todos: []
}

const todoReducer = (state=initialState, action = {}) => {

  switch (action.type) {
    
    case action_types.SET_TODOS_FROM_FIREBASE:
        return Object.assign({}, state, {todos: _.sortBy(action.payload, todo => todo.status === DONE_STATUS)});

    case action_types.ADD_TODO:      
        return Object.assign({}, state, {currentTodo:''});

    case action_types.REMOVE_TODO:        
        return Object.assign({}, state, {currentTodo:''});

    case action_types.CHANGE_TEXTFIELD:
      if (action.payload) {
        return Object.assign({}, state, {currentTodo:action.payload});
      }
      return state;

    case action_types.REMOVE_SELECTED_TODOS:
        return Object.assign({}, state, {currentTodo:''});

    case action_types.TOGGLE_TODO_STATUS:
        return state;
  
    default:
      return state;
  }
}

export default todoReducer;